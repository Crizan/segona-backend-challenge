<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //identify associated user id
        $user_id = auth()->user()->id;
        $user = User::find($user_id);

        $data = array(
            //send items of particular user to dashboard
            'items' => $user->items,
            //send customers of particular user to dashboard
            'customers' => $user->customers
        );
        return view('home')->with($data);
    }
}
