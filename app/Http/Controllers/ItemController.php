<?php

namespace App\Http\Controllers;

use App\Model\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //retrieve 5 items per page
        $items= Item::orderBy('created_at','desc')->paginate(5);
        return view('todolist\Item\items')->with('items',$items);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('todolist\Item\create');
    }

    //search for items
    public function search(Request $request){
        $search = $request->get('search');
        $items=  Item::where('name','like','%'.$search.'%')->paginate(5);
        return view('todolist\Item\items',['items'=>$items]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate form
        $this->validate($request , [
            'name' => 'required|unique:items',
            'description' => 'required',
            'created_date' => 'required',
            'image' => 'image|nullable|max:1999'
        ]);

        //Handle Image Upload
        if($request->hasFile('image')){
            //Get filename with the extension
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            //Get Just Filename
            $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
            //Get just Ext
            $extension = $request->file('image')->getClientOriginalExtension();
            //Filename to Store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            //Upload Image
            $path = $request->file('image')->storeAs('public/images',$fileNameToStore);
        }else{
            $fileNameToStore='noimage.jpg';
        }

        //Create Item
        $item = new Item;
        $item->name = $request->input('name');
        $item->description = $request->input('description');
        $item->created_date = $request->input('created_date');
        $item->image = $fileNameToStore;
        $item->user_id= auth()->user()->id;

        $item->save();

        return redirect('/items')->with('success','Item Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        return view('todolist\Item\edit')->with('item',$item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request , [
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|nullable|max:1999'
        ]);

        //Handle Image Upload
        if($request->hasFile('image')){
            //Get filename with the extension
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            //Get Just Filename
            $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
            //Get just Ext
            $extension = $request->file('image')->getClientOriginalExtension();
            //Filename to Store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            //Upload Image
            $path = $request->file('image')->storeAs('public/images',$fileNameToStore);
        }else{
            $fileNameToStore='noimage.jpg';
        }

        //Create Item
        $item = Item::find($id);
        $item->name = $request->input('name');
        $item->description = $request->input('description');
        
        $item->image = $fileNameToStore;

        $item->save();

        return redirect('/items')->with('success','Item Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find item by id
        $item= Item::find($id);
        //delete item selected
        $item->delete();
        return redirect('/items')->with('success','Item Deleted');
    }
}
