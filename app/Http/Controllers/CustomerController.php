<?php

namespace App\Http\Controllers;

use App\Model\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //retrieve seven customers per page
        $customer= Customer::orderBy('created_at','desc')->paginate(7);

        return view('todolist\Customer\customer')->with('customer',$customer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('todolist\Customer\create');
    }

    //search for customer
    public function search1(Request $request){
        $search = $request->get('search');
        $customer=  Customer::where('name','like','%'.$search.'%')->orwhere('contact_number','like','%'.$search.'%')->paginate(5);
        return view('todolist\Customer\customer',['customer'=>$customer]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate form
        $this->validate($request , [
            'name' => 'required|unique:customers',
            'gender' => 'required',
            'contact_number' => 'required|unique:customers',
            'email' => 'required|email|unique:customers',
            'age' => 'required|integer|between:15,45',
            'item' => 'required'
            
        ]);

        //Create Customer
        $customer = new Customer;
        $customer->name = $request->input('name');
        $customer->gender = $request->input('gender');
        $customer->contact_number = $request->input('contact_number');
        $customer->email = $request->input('email');
        $customer->age = $request->input('age');
        $customer->item = $request->input('item');
        $customer->user_id= auth()->user()->id;

        $customer->save();

        return redirect('/customers')->with('success','Customer Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        return view('todolist\Customer\edit')->with('customer',$customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate form
        $this->validate($request , [
            'name' => 'required',
            'gender'=>'required',
            'contact_number' => 'required',
            'email' => 'required|email',
            'age' => 'required|integer|between:15,45',
            'item' => 'required'
        ]);

        //Create Customer
        $customer = Customer::find($id);
        $customer->name = $request->input('name');
        $customer->gender = $request->input('gender');
        $customer->contact_number = $request->input('contact_number');
        $customer->email = $request->input('email');
        $customer->age = $request->input('age');
        $customer->item = $request->input('item');

        $customer->save();

        return redirect('/customers')->with('success','Customer Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find customer by id
        $customer= Customer::find($id);
        //delete customer selected
        $customer->delete();
        return redirect('/customers')->with('success','Customer Deleted');
    }
}
