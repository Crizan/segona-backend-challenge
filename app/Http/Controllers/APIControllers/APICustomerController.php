<?php

namespace App\Http\Controllers\APIControllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\Customer;
use App\Http\Resources\Customer as CustomerResource;
use App\Http\Controllers\Controller;

class APICustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get customers
        $customer= Customer::paginate(4);

        //Return Collection of customers as resource
        return CustomerResource::collection($customer);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //initialize new customer
         $customer =  new Customer;

         //get request body
         $customer->id = $request->input('id');
         $customer->name = $request->input('name');
         $customer->gender = $request->input('gender');
         $customer->contact_number = $request->input('contact_number');
         $customer->email = $request->input('email');
         $customer->age = $request->input('age');
         $customer->item = $request->input('item');
 
         //save to database
         if($customer->save()){
             return new CustomerResource($customer);
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         //Get customer
         $customer = Customer::findOrFail($id);
         return new CustomerResource($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        //get request body
        $customer->name = $request->input('name');
        $customer->gender = $request->input('gender');
        $customer->contact_number = $request->input('contact_number');
        $customer->email = $request->input('email');
        $customer->age = $request->input('age');
        $customer->item = $request->input('item');

        //save to database
        if($customer->save()){
            return new CustomerResource($customer);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Get customer
        $customer = Customer::findOrFail($id);

        //delete customer
        if($customer->delete()){
            return new CustomerResource($customer);
        }
    }
}
