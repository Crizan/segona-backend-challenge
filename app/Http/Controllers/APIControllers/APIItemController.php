<?php

namespace App\Http\Controllers\APIControllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\Item;
use App\Http\Resources\Item as ItemResource;
use App\Http\Controllers\Controller;

class APIItemController extends Controller
{
    /**
     * Display a listing of the  resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get items
        $items= Item::paginate(4);

        //Return Collection of items as resource
        return ItemResource::collection($items);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //initialize new item
        $item =  new Item;

        //get request body
        $item->id = $request->input('id');
        $item->name = $request->input('name');
        $item->description = $request->input('description');
        $item->created_date = $request->input('created_date');
        $item->image = $request->input('image');

        //save to database
        if($item->save()){
            return new ItemResource($item);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Get item
        $item = Item::findOrFail($id);
        return new ItemResource($item);
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Item $item)
    {       
        //get request body
        $item->name = $request->input('name');
        $item->description = $request->input('description');
        $item->created_date = $request->input('created_date');
        $item->image = $request->input('image');

        //save to database
        if($item->save()){
            return new ItemResource($item);
        }
    }

    
    public function destroy($id)
    {
        //Get item
        $item = Item::findOrFail($id);

        //delete item
        if($item->delete()){
            return new ItemResource($item);
        }
    }
}
