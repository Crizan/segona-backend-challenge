# segona-backend-challenge

<b>Segona Backend Challenge is simple todo list web application with following feature:</b>

Register Page:
      ![register](/uploads/ad59dbed0b8f79d0887acdf9bbefc590/register.PNG)

Login only by registered user:
    ![login](/uploads/aacdd0574c9f067ea826aea48623a0a2/login.PNG)

<b>CRUD operation for Item:</b>

 View:
![items](/uploads/8f5ceed2e0007d352096eeec59062152/items.PNG)
 Create:
![itemscreate](/uploads/c6744d1865a2c01740188d50dac6a7ab/itemscreate.PNG)
 Update:
![itemsupdate](/uploads/c955cb8e67515d67bf0e3d1a0f762b68/itemsupdate.PNG)
 Delete:
 ![itemsdelete](/uploads/bfa6a07da03f74c20e7d8613218bf307/itemsdelete.PNG)
 
 
<b>CRUD operation for Customer:</b>

View:
![customers](/uploads/d6a31840955f470aedd747845996f9a2/customers.PNG)

 Create:
 ![customerscreate](/uploads/9d437028f6d3358e8e096d1c6dee8786/customerscreate.PNG)

 Update:
 ![customersudpate](/uploads/f0857d0b3f3d5beb84789163de7d9086/customersudpate.PNG)

 Delete:
 ![customersdelete](/uploads/43b277432c8226481f20f1d156d17442/customersdelete.PNG)
 
    

<b> The API CRUD application screenshots are:</b>

<b>CRUD operation for Item:</b>

 View:
 ![items](/uploads/591958c723996c8d29d0dc23670335bc/items.PNG)
 
 View by ID:
 ![itembyid](/uploads/5cbef5ef306c1ed66cd2708f931127e6/itembyid.PNG)

 Store:
 ![itemsstore](/uploads/c9e3fc1696d030e10b4a68412358bdcc/itemsstore.PNG)

 Update:
 ![itemsupdate](/uploads/7f7d76a2a0b051cacf7e6d362d8b53f3/itemsupdate.PNG)

 Delete:
 ![itemsdelete](/uploads/64054d66f17c0be12ec52d4003e24f9d/itemsdelete.PNG)
 
 
<b>CRUD operation for Customer:</b>

View:
![customers](/uploads/730dcfaa20fd92b6c433f135054ad87e/customers.PNG)

View by ID:
![customerbyid](/uploads/02b95ab2e3220b7fde2c5d8d140e15c1/customerbyid.PNG)

 Store:
 ![customerstore](/uploads/d2efd81c5b7d4d95d5813085d99ec6ee/customerstore.PNG)
 

 Update:
 ![customerupdate](/uploads/e8d494d7d86b7bf629deb3d05bc1bad7/customerupdate.PNG)

 Delete:
 ![customerdelete](/uploads/88bd4d0b9b45465268620a04d5d64f71/customerdelete.PNG)
 
 

