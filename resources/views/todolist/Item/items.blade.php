@extends('layouts.app')

@section('content')

<div class="container">
  <div class="col-md-6">
      <form action="/search" method="GET">
        <div class="input-group">
          <input type="search" name="search" class="form-control">
            <span class="input-group-prepand">
              <button type="submit" class="btn btn-primary">Search</button>
            </span>
            <a href="/items/create" class="btn btn-primary" style="margin-left:23px;">Create Item</a>
        </div>
        
      </form>
      
  </div>
  
        <br />   
        <table class="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Created Date</th>
            <th>Image</th>
            <th colspan="2">Action</th>
          </tr>
        </thead>
        <tbody>
          
          @foreach($items as $item)
        
          <tr>
            <td>{{$item['id']}}</td>
            <td>{{$item['name']}}</td>
            <td>{{$item['description']}}</td>
            <td>{{$item['created_date']}}</td>
            <td>{{$item['image']}}</td>
            <td><a href="{{action('ItemController@edit', $item['id'])}}" class="btn btn-warning">Edit</a></td>
                        <td>
                          <form action="{{action('ItemController@destroy', $item['id'])}}" method="post">
                            @csrf
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-danger" type="submit">Delete</button>
                          </form>
                        </td>
          </tr>

          @endforeach
          
        </tbody>
      </table>
      {{$items->links()}}
    
</div>  



    
@endsection