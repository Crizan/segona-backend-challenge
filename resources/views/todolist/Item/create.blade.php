@extends('layouts\app')


@section('content')
<div class="container">
        <h2>Items </h2><br/>
        <form method="post" action="{{action('ItemController@store')}}" enctype="multipart/form-data">
          @csrf
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Name">Name:</label>
              <input type="text" class="form-control" name="name">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4"></div>
              <div class="form-group col-md-4">
                <label for="description">Description:</label>
                <input type="text" class="form-control" name="description">
              </div>
            </div>
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label>Created Date : </label>  
              <input type="text"  name="created_date" class="date-picker form-control">  
           </div>
          </div>
          <div class="row">
                  <div class="col-md-4"></div>
                  <div class="form-group col-md-4">
                          <label>Image : </label>  
                    <input type="file" name="image">    
                 </div>
                </div>
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4" style="margin-top:60px">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </div>
        </form>
      </div>
      <script type="text/javascript">  
          $(".date-picker").nepaliDatePicker({
            dateFormat: "%D, %M %d, %y",
            closeOnDateSelect: true
        }).val(); 

      </script>
    
@endsection