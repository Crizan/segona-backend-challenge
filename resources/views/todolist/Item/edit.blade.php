@extends('layouts\app')


@section('content')
<div class="container">
        <h2>Items </h2><br/>
        <form method="post" action="{{action('ItemController@update', $item)}}" enctype="multipart/form-data">
          @csrf
          <input name="_method" type="hidden" value="PATCH">
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Name">Name:</label>
              <input type="text" class="form-control" name="name" value="{{$item->name}}">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4"></div>
              <div class="form-group col-md-4">
                <label for="description">Description:</label>
                <input type="textarea" class="form-control" name="description" value="{{$item->description}}">
              </div>
            </div>
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label>Created Date : </label>  
              <input type="text"  name="date" class="date-picker form-control" value="{{$item->created_date}}">
           </div>
                   
          </div>
          <div class="row">
                  <div class="col-md-4"></div>
                  <div class="form-group col-md-4">
                          <label>Image : </label>  
                    <input type="file" name="filename" value="{{$item->image}}">    
                 </div>
                </div>
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4" style="margin-top:60px">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </div>
        </form>
      </div>
      <script type="text/javascript">  
        $(".date-picker").nepaliDatePicker({
          dateFormat: "%D, %M %d, %y",
          closeOnDateSelect: true
      });      

    </script>
    
@endsection