@extends('layouts.app')

@section('content')

<div class="container">
        
        <div class="col-md-6">
            <form action="/search1" method="GET">
              <div class="input-group">
                <input type="search" name="search" class="form-control">
                  <span class="input-group-prepand">
                    <button type="submit" class="btn btn-primary">Search</button>
                  </span>
                  <a href="/customers/create" class="btn btn-primary" style="margin-left:27px;">Create Customer</a>
              </div>
            </form>
        </div>
        <br />
        <table class="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Contact Number</th>
            <th>Email</th>
            <th>Age</th>
            <th>Item</th>
            <th colspan="2">Action</th>
          </tr>
        </thead>
        <tbody>
          
          @foreach($customer as $customer)
        
          <tr>
            <td>{{$customer['id']}}</td>
            <td>{{$customer['name']}}</td>
            <td>{{$customer['gender']}}</td>
            <td>{{$customer['contact_number']}}</td>
            <td>{{$customer['email']}}</td>
            <td>{{$customer['age']}}</td>
            <td>{{$customer['item']}}</td>
            <td><a href="{{action('CustomerController@edit', $customer['id'])}}" class="btn btn-warning">Edit</a></td>
            <td>
              <form action="{{action('CustomerController@destroy', $customer['id'])}}" method="post">
                @csrf
                <input name="_method" type="hidden" value="DELETE">
                <button class="btn btn-danger" type="submit">Delete</button>
              </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      
</div>  



    
@endsection