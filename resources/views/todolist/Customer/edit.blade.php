@extends('layouts\app')


@section('content')
<div class="container">
        <h2>Customers </h2><br/>
        <form method="post" action="{{action('CustomerController@update',$customer)}}" enctype="multipart/form-data">
          @csrf
          <input name="_method" type="hidden" value="PATCH">
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Name">Name:</label>
              <input type="text" class="form-control" name="name" value="{{$customer->name}}">
            </div>
          </div>
          <div class="row">
              <div class="col-md-4"></div>
              <div class="form-group col-md-4">
              <label style="margin-right:17px;">Gender:</label>
                  <input type="radio" id="male" name="gender" value="M"  checked>Male<span style="margin-right:17px;"></span>
                  <input type="radio" id="female" name="gender" value="F" >Female
              </div>
            </div>
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label>Contact Number: </label>  
              <input type="text" name="contact_number" class="form-control" value="{{$customer->contact_number}}">  
           </div>
          </div>
          <div class="row">
              <div class="col-md-4"></div>
              <div class="form-group col-md-4">
                <label>Email: </label>  
                <input type="text" name="email" class="form-control" value="{{$customer->email}}">  
             </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                  <label>Age: </label>  
                  <input type="text" name="age" class="form-control" value="{{$customer->age}}">  
               </div>
              </div>
              <div class="row">
                  <div class="col-md-4"></div>
                  <div class="form-group col-md-4">
                      <label>Item: </label>  
                      <select name="item" id="item" class="form-control">
                          <option value="Food" selected >Food</option>
                          <option value="Pencil">Pencil</option>
                          <option value="Fruits">Fruits</option>
                    </select>
                 </div>
                </div>
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4" style="margin-top:60px">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </div>
        </form>
      </div>
 
    
@endsection