@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif                       
                        <br><br>

                        <a href="/items" class="btn btn-primary" ">Item</a>
                        <a href="/customers" class="btn btn-primary" ">Customer</a>



                    {{-- <h3>Your Items <a href="/items/create" class="btn btn-primary" style="float:right;">Create Item</a></h3> 
                        
                    <br />

                    <div class="col-md-6">
                        <form action="/search" method="GET">
                          <div class="input-group">
                            <input type="search" name="search" class="form-control">
                              <span class="input-group-prepand">
                                <button type="submit" class="btn btn-primary">Search</button>
                              </span>
                          </div>
                        </form>
                  <br>
                    </div>
      
                    <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Created Date</th>
                        <th>Image</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                      @foreach($items as $item)
                    
                      <tr>
                        <td>{{$item['id']}}</td>
                        <td>{{$item['name']}}</td>
                        <td>{{$item['description']}}</td>
                        <td>{{$item['created_date']}}</td>
                        <td>{{$item['image']}}</td>
                        
                        <td><a href="{{action('ItemController@edit', $item['id'])}}" class="btn btn-warning">Edit</a></td>
                        <td>
                          <form action="{{action('ItemController@destroy', $item['id'])}}" method="post">
                            @csrf
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-danger" type="submit">Delete</button>
                          </form>
                        </td>
                      </tr>
                      @endforeach
                      
                    </tbody>
                  </table>
                    <hr><hr>
                  <h3>Your Customers<a href="/customers/create" class="btn btn-primary" style="float:right;">Create Customer</a></h3>
                  <br />
      
        <table class="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Contact Number</th>
            <th>Email</th>
            <th>Age</th>
            <th>Item</th>
            <th colspan="2">Action</th>
          </tr>
        </thead>
        <tbody>

            <div class="col-md-6">
                <form action="/search1" method="GET">
                  <div class="input-group">
                    <input type="search" name="search" class="form-control">
                      <span class="input-group-prepand">
                        <button type="submit" class="btn btn-primary">Search</button>
                      </span>
                  </div>
                </form>
          
            </div>
            <br>
          
          @foreach($customers as $customer)
        
          <tr>
            <td>{{$customer['id']}}</td>
            <td>{{$customer['name']}}</td>
            <td>{{$customer['gender']}}</td>
            <td>{{$customer['contact_number']}}</td>
            <td>{{$customer['email']}}</td>
            <td>{{$customer['age']}}</td>
            <td>{{$customer['item']}}</td>
            
            <td><a href="{{action('CustomerController@edit', $customer['id'])}}" class="btn btn-warning">Edit</a></td>
            <td>
              <form action="{{action('CustomerController@destroy', $customer['id'])}}" method="post">
                @csrf
                <input name="_method" type="hidden" value="DELETE">
                <button class="btn btn-danger" type="submit">Delete</button>
              </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
                </div> --}}
            </div>
        </div>
    </div>
</div>
@endsection
